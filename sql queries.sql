-- Exercise: 1 (Serge I: 2002-09-30) --

/* Find the model number, speed and hard drive capacity
 * for all the PCs with prices below $500.
 * Result set: model, speed, hd.
 *
	SELECT model, speed, hd
	FROM PC
	WHERE price < 500
 *
 */

-- Exercise: 2 (Serge I: 2002-09-21) --

/* List all printer makers.
 * Result set: maker.
 *
	SELECT DISTINCT maker AS [Maker]
	FROM Product
	WHERE type = 'Printer'
 *
 */

-- Exercise: 3 (Serge I: 2002-09-30) --

/* Find the model number, RAM and screen size
 * of the laptops with prices over $1000.
 *
	SELECT model, ram, screen
	FROM Laptop
	WHERE price > 1000
 *
 */

-- Exercise: 4 (Serge I: 2002-09-21) --

/* Find all records from the Printer table containing
 * data about color printers.
 *
	SELECT *
	FROM Printer
	WHERE color = 'y'
 *
 */

-- Exercise: 5 (Serge I: 2002-09-30) --

/* Find the model number, speed and hard drive
 * capacity of PCs cheaper than $600
 * having a 12x or a 24x CD drive. 
 *
	SELECT model, speed, hd
	FROM PC
	WHERE cd IN('24x', '12x') AND price < 600
 *
 */

-- Exercise: 6 (Serge I: 2002-10-28) --

/* For each maker producing laptops with a hard drive capacity 
 * of 10 Gb or higher, find the speed of such laptops.
 * Result set: maker, speed. 
 *
	SELECT DISTINCT maker, speed
	FROM Product INNER JOIN Laptop
	ON Product.model = Laptop.model
	WHERE hd >= 10
	ORDER BY maker, speed
 *
 */

-- Exercise: 7 (Serge I: 2002-11-02) --

/* Get the models and prices for all commercially available
 * products (of any type) produced by maker B. 
 *
	WITH ModelsAndPrices AS (
		SELECT model, price
		FROM PC
			UNION
		SELECT model, price
		FROM Laptop
			UNION
		SELECT model, price
		FROM Printer
	)

	SELECT ModelsAndPrices.model, ModelsAndPrices.price
	FROM Product INNER JOIN ModelsAndPrices
	ON Product.model = ModelsAndPrices.model
	WHERE maker = 'B'
 *
 */

-- Exercise: 8 (Serge I: 2003-02-03) --

/* Find the makers producing PCs but not laptops. 
 *
	SELECT maker AS [Maker]
	FROM Product
	WHERE type = 'PC'
	EXCEPT
	SELECT maker AS [Maker]
	FROM Product
	WHERE type = 'Laptop'
 *
 */

-- Exercise: 9 (Serge I: 2002-11-02) --

/* Find the makers of PCs with a processor
 * speed of 450 MHz or more.
 * Result set: maker. 
 *
	SELECT DISTINCT maker AS [Maker]
	FROM Product INNER JOIN PC
	ON Product.model = PC.model
	WHERE speed >= 450
 *
 */

-- Exercise: 10 (Serge I: 2002-09-23) --

/* Find the printer models having the highest price.
 * Result set: model, price. 
 *
	SELECT TOP(1) WITH TIES model, price
	FROM Printer
	ORDER BY price DESC
 *
 */

-- Exercise: 11 (Serge I: 2002-11-02) --

/* Find out the average speed of PCs.
 *
	SELECT AVG(speed) AS [Avg_speed]
	FROM PC
 *
 */

-- Exercise: 12 (Serge I: 2002-11-02) --

/* Find out the average speed
 * of the laptops priced over $1000.
 *
	SELECT AVG(speed) AS [Avg_speed]
	FROM Laptop
	WHERE price > 1000
 *
 */

-- Exercise: 13 (Serge I: 2002-11-02) --

/* Find out the average speed of the PCs produced by maker A.
 *
	SELECT AVG(speed) AS [Avg_speed]
	FROM Product INNER JOIN PC
	ON Product.model = PC.model
	WHERE maker = 'A'
 *
 */

-- Exercise: 15 (Serge I: 2003-02-03) --

/* Get hard drive capacities that
 * are identical for two or more PCs. 
 * Result set: hd.
 *
	SELECT hd
	FROM PC
	GROUP BY hd
	HAVING COUNT(*) >= 2
 *
 */

-- Exercise: 16 (Serge I: 2003-02-03) --

/* Get pairs of PC models with identical speeds and
 * the same RAM capacity. Each resulting pair should 
 * be displayed only once, i.e. (i, j) but not (j, i).
 * Result set: model with the bigger number, model
 * with the smaller number, speed, and RAM. 
 *
	SELECT DISTINCT pc1.model, pc2.model, pc1.speed, pc1.ram
	FROM PC AS pc1 INNER JOIN PC AS pc2
	ON pc1.speed = pc2.speed
	   AND pc1.ram = pc2.ram
       AND pc1.model > pc2.model
 *
 */

-- Exercise: 18 (Serge I: 2003-02-03) --

/* Find the makers of the cheapest color printers.
 * Result set: maker, price.
 *
	SELECT DISTINCT maker AS [Maker], price
	FROM Product INNER JOIN Printer
	ON Product.model = Printer.model
	WHERE price = (
		SELECT MIN(price)
		FROM Printer
		WHERE color = 'y'
	) AND color = 'y'
 *
 */

-- Exercise: 19 (Serge I: 2003-02-13) --

/* For each maker having models in the Laptop table, find 
 * out the average screen size of the laptops he produces.
 * Result set: maker, average screen size. 
 *
	SELECT maker AS [Maker], AVG(screen) AS [Avg_screen]
	FROM Product INNER JOIN Laptop
	ON Product.model = Laptop.model
	GROUP BY maker
 *
 */

-- Exercise: 20 (Serge I: 2003-02-13) --

/* Find the makers producing at least three distinct models of PCs.
 * Result set: maker, number of PC models.
 *
	SELECT maker AS [Maker], COUNT(*) AS [Count_Model]
	FROM Product
	WHERE type = 'PC'
	GROUP BY maker
	HAVING COUNT(*) >= 3
 *
 */

-- Exercise: 21 (Serge I: 2003-02-13) --

/* Find out the maximum PC price for each
 * maker having models in the PC table.
 * Result set: maker, maximum price.
 *
	SELECT maker AS [Maker], MAX(price) AS [Max_price]
	FROM Product INNER JOIN PC
	ON Product.model = PC.model
	GROUP BY maker
 *
 */

-- Exercise: 22 (Serge I: 2003-02-13) --

/* For each value of PC speed that exceeds 600 MHz,
 * find out the average price of PCs with identical speeds.
 * Result set: speed, average price.
 *
	SELECT speed, AVG(price) AS [Avg_price]
	FROM PC
	WHERE speed > 600
	GROUP BY speed
 *
 */

-- Exercise: 23 (Serge I: 2003-02-14) --

/* Get the makers producing both PCs having a speed of 750 MHz 
 * or higher and laptops with a speed of 750 MHz or higher. 
 * Result set: maker 
 *
	SELECT maker
	FROM Product INNER JOIN PC 
	ON Product.model = PC.model
	WHERE speed>=750 
		INTERSECT
	SELECT maker
	FROM Product INNER JOIN Laptop
	ON Product.model = Laptop.model
	WHERE speed>=750 
 *
 */

-- Exercise: 24 (Serge I: 2003-02-03) --

/* List the models of any type having the highest price
 * of all products present in the database.
 *
	WITH modelPrices AS (
		SELECT model, price FROM PC
		UNION
		SELECT model, price FROM Laptop
		UNION
		SELECT model, price FROM Printer
	)

	SELECT TOP(1) WITH TIES model 
	FROM modelPrices
	ORDER BY price DESC
 *
 */

-- Exercise: 25 (Serge I: 2003-02-14) --

/* Find the printer makers also producing PCs with the lowest
 * RAM capacity and the highest processor speed of all PCs
 * having the lowest RAM capacity.
 * Result set: maker. 
 *
	SELECT DISTINCT maker
	FROM Product INNER JOIN PC
	ON Product.model = PC.model
	WHERE maker IN (
		SELECT maker
		FROM Product
		WHERE type = 'Printer'
	) AND ram IN (
		SELECT MIN(ram)
		FROM PC
	) AND speed IN (
		SELECT MAX(speed)
		FROM PC
		WHERE ram IN (
			SELECT MIN(ram)
			FROM PC
		)
	)
 *
 */

-- Exercise: 26 (Serge I: 2003-02-14) --

/* Find out the average price of PCs
 * and laptops produced by maker A.
 * Result set: one overall average price for all items. 
 *
	WITH ModelsAndPrices AS (
		SELECT model, price
		FROM PC
			UNION ALL
		SELECT model, price
		FROM Laptop
	)

	SELECT AVG(price) AS [AVG_price]
	FROM Product INNER JOIN ModelsAndPrices
	ON Product.model = ModelsAndPrices.model
	WHERE maker = 'A'
 *
 */

-- Exercise: 27 (Serge I: 2003-02-03) --

/* Find out the average hard disk drive capacity of PCs
 * produced by makers who also manufacture printers.
 * Result set: maker, average HDD capacity. 
 *
	SELECT maker AS [Maker], AVG(hd) AS [Avg_hd]
	FROM Product INNER JOIN PC
	ON Product.model = PC.model
	WHERE maker IN (
		SELECT maker
		FROM Product
		WHERE type = 'printer'
	)
	GROUP BY maker
 *
 */

-- Exercise: 28 (Serge I: 2012-05-04) --

/* Using Product table, find out the number of 
 * makers who produce only one model. 
 *
	WITH distinctMakers AS (
		SELECT maker
		FROM Product
		GROUP BY maker
		HAVING COUNT(model) = 1
	)

	SELECT COUNT(*) AS [qty]
	FROM distinctMakers
 *
 */

-- Exercise: 29 (Serge I: 2003-02-14) --

/* Under the assumption that receipts of money (inc) and payouts (out)
 * are registered not more than once a day for each collection point 
 * [i.e. the primary key consists of (point, date)], write a query
 * displaying cash flow data (point, date, income, expense). 
 * Use Income_o and Outcome_o tables. 
 *
	SELECT ISNULL(Income_o.point, Outcome_o.point) AS [point],
		   ISNULL(Income_o.date, Outcome_o.date) AS [date],
	       inc,
	       out
	FROM Income_o FULL JOIN Outcome_o
	ON Income_o.point = Outcome_o.point
       AND Income_o.date = Outcome_o.date
 *
 */

-- Exercise: 30 (Serge I: 2003-02-14) --

/* Under the assumption that receipts of money (inc) and payouts (out)
 * can be registered any number of times a day for each collection point
 * [i.e. the code column is the primary key], display a table with one
 * corresponding row for each operating date of each collection point.
 * Result set: point, date, total payout per day (out), total money intake per day (inc).
 * Missing values are considered to be NULL. 
 *
	WITH IncomeAndOutcome AS (
		SELECT point, date, NULL AS [out], inc
		FROM Income
		UNION ALL
		SELECT point, date, out, NULL AS [inc]
		FROM Outcome
	)

	SELECT point, date, SUM(out) AS [Outcome], SUM(inc) AS [Income]
	FROM IncomeAndOutcome
	GROUP BY point, date
 *
 */

-- Exercise: 31 (Serge I: 2002-10-22) --

/* For ship classes with a gun caliber of 16 in.
 * or more, display the class and the country. 
 *
	SELECT class, country
	FROM Classes
	WHERE bore >= 16
 *
 */

-- Exercise: 32 (Serge I: 2003-02-17) --

/* One of the characteristics of a ship is one-half the cube of 
 * the calibre of its main guns (mw). Determine the average ship
 * mw with an accuracy of two decimal places for each country
 * having ships in the database. 
 *
	WITH CountriesAndBores AS (
		SELECT country, bore, name
		FROM Classes LEFT JOIN Ships
		ON Classes.class = Ships.class
			UNION ALL
		SELECT DISTINCT country, bore, ship
		FROM Classes LEFT JOIN Outcomes
		ON Classes.class = Outcomes.ship
		WHERE ship NOT IN (
			SELECT name
			FROM Ships
		)
	)

	SELECT country, FORMAT(AVG(POWER(bore, 3) / 2), N'0.00')  AS [weight]
	FROM CountriesAndBores
	WHERE name IS NOT NULL
	GROUP BY country	
 *
 */

-- Exercise: 33 (Serge I: 2002-11-02) --

/* Get the ships sunk in the North Atlantic battle. 
 * Result set: ship. 
 *
	SELECT ship
	FROM Outcomes
	WHERE battle = 'North Atlantic' AND result = 'sunk'
 *
 */

-- Exercise: 34 (Serge I: 2002-11-04) --

/* In accordance with the Washington Naval Treaty concluded
 * in the beginning of 1922, it was prohibited to build battle
 * ships with a displacement of more than 35 thousand tons. 
 * Get the ships violating this treaty (only consider ships
 * for which the year of launch is known).
 * List the names of the ships.
 *
	SELECT name
	FROM Ships
	WHERE class IN (
		SELECT class
		FROM Classes
		WHERE type = 'bb'
			  AND displacement > 35000
	) AND launched >= 1922
 *
 */

-- Exercise: 35 (qwrqwr: 2012-11-23) --

/* Find models in the Product table consisting either
 * of digits only or Latin letters (A-Z, case insensitive) only.
 * Result set: model, type.
 *
	SELECT model, type 
	FROM Product
	WHERE model NOT LIKE '%[^0-9]%' OR model NOT LIKE '%[^A-Z]%'
 *
 */

-- Exercise: 36 (Serge I: 2003-02-17) --

/* List the names of lead ships in the
 * database (including the Outcomes table).
 *
	SELECT class
	FROM Ships
	WHERE class = name
		UNION
	SELECT Classes.class
	FROM Classes INNER JOIN Outcomes
	ON Classes.class = Outcomes.ship
 *
 */

-- Exercise: 37 (Serge I: 2003-02-17) --

/* Find classes for which only one ship exists
 * in the database (including the Outcomes table).
 *
	WITH ClassesAndNames AS(
		SELECT class, name
		FROM Ships
			UNION
		SELECT Classes.class, Outcomes.ship
		FROM Classes INNER JOIN Outcomes
		ON Classes.class = Outcomes.ship
	)

	SELECT class
	FROM ClassesAndNames
	GROUP By class
	HAVING COUNT(name) = 1
 *
 */

-- Exercise: 38 (Serge I: 2003-02-19) --

/* Find countries that ever had classes
 * of both battleships (�bb�) and cruisers (�bc�).
 *
	SELECT country
	FROM Classes
	GROUP BY country
	HAVING COUNT(DISTINCT type) = 2
 *
 */

-- Exercise: 40 (Serge I: 2012-04-20) --

/* Get the makers who produce only one product type
 * and more than one model.
 * Output: maker, type. 
 *
	SELECT maker, MAX(type) AS [type]
	FROM Product
	GROUP BY maker
	HAVING COUNT(DISTINCT type) = 1 AND COUNT(model) > 1
 *
 */

-- Exercise: 42 (Serge I: 2002-11-05) --

/* Find the names of ships sunk at battles,
 * along with the names of the corresponding battles.
 *
	SELECT ship, battle
	FROM Outcomes
	WHERE result = 'sunk'
 *
 */

-- Exercise: 43 (qwrqwr: 2011-10-28) --

/* Get the battles that occurred in years when
 * no ships were launched into water.
 *
	SELECT name
	FROM Battles
	WHERE YEAR(date) NOT IN(
		SELECT DISTINCT launched
		FROM Ships
		WHERE launched IS NOT NULL
	)
 *
 */

 -- Exercise: 44 (Serge I: 2002-12-04) --

/* Find all ship names beginning with the letter R.
 *
	SELECT ship AS [name]
	FROM Outcomes
	WHERE ship LIKE 'R%'
		UNION
	SELECT name AS [name]
	FROM Ships
	WHERE name LIKE 'R%'
 *
 */

  -- Exercise: 45 (Serge I: 2002-12-04) --

/* Find all ship names consisting of three or more words (e.g., King George V).
 * Consider the words in ship names to be separated by single spaces,
 * and the ship names to have no leading or trailing spaces. 
 *
	SELECT ship AS [name]
	FROM Outcomes
	WHERE ship LIKE '% % %'
		UNION
	SELECT name AS [name]
	FROM Ships
	WHERE name LIKE '% % %'
 *
 */

-- Exercise: 49 (Serge I: 2003-02-17) --

/* Find the names of the ships having a gun caliber
 * of 16 inches (including ships in the Outcomes table).
 *
	SELECT name
	FROM Classes INNER JOIN Ships
	ON Classes.class = Ships.class
	WHERE bore = 16
		UNION
	SELECT ship
	FROM Classes INNER JOIN Outcomes
	ON Classes.class = Outcomes.ship
	WHERE bore = 16
 *
 */

-- Exercise: 50 (Serge I: 2002-11-05) --

/* Find the battles in which Kongo-class ships
 * from the Ships table were engaged.
 *
	SELECT DISTINCT battle
	FROM Ships INNER JOIN Outcomes
	ON Ships.name = Outcomes.ship
	WHERE class = 'Kongo'
 *
 */

-- Exercise: 51 (Serge I: 2003-02-17) --

/* Find the names of the ships with the largest number
 * of guns among all ships having the same displacement
 * (including ships in the Outcomes table). 
 *
	WITH GunsAndDisplacements AS (
		SELECT name, numGuns, displacement
		FROM Classes INNER JOIN Ships
		ON Classes.class = Ships.class
			UNION
		SELECT ship, numGuns, displacement
		FROM Classes INNER JOIN Outcomes
		ON Classes.class = Outcomes.ship
	)

	SELECT name
	FROM GunsAndDisplacements AS OUTER_DATA
	WHERE numGuns = (
		SELECT MAX(numGuns)
		FROM GunsAndDisplacements AS INNER_DATA
		WHERE INNER_DATA.displacement = OUTER_DATA.displacement
	)
 *
 */

-- Exercise: 52 (qwrqwr: 2010-04-23) --

/* Determine the names of all ships in the Ships table that can be a 
 * Japanese battleship having at least nine main guns with a caliber 
 * of less than 19 inches and a displacement of not more than 65 000 tons. 
 *	
	SELECT name
	FROM Classes INNER JOIN Ships
	ON Classes.class = Ships.class
	WHERE type = 'bb' 
		  AND country ='Japan'
		  AND (numGuns >= 9 OR numGuns IS NULL)
		  AND (bore < 19 OR bore IS NULL)
		  AND (displacement <= 65000 OR displacement IS NULL)
 *
 */

-- Exercise: 53 (Serge I: 2002-11-05) --

/* With a precision of two decimal places, determine
 * the average number of guns for the battleship classes. 
 *
	SELECT FORMAT(AVG(CAST(numGuns AS float)), '0.00') AS [Avg-numGuns]	
	FROM Classes
	WHERE type = 'bb'
	GROUP BY type
 *
 */

-- Exercise: 55 (Serge I: 2003-02-16) --

/* For each class, determine the year the first ship of this class
 * was launched. If the lead ship�s year of launch is not known, 
 * get the minimum year of launch for the ships of this class.
 * Result set: class, year.
 *
	SELECT Classes.class, MIN(launched) AS [year]
	FROM Classes LEFT JOIN Ships
	ON Classes.class = Ships.class
	GROUP BY Classes.class
 *
 */

-- Exercise: 56 (Serge I: 2003-02-16) --

/* For each class, find out the number of ships of
 * this class that were sunk in battles.
 * Result set: class, number of ships sunk.
 *
	WITH OutcomeClasses AS (
		SELECT Outcomes.ship, Ships.class
		FROM Outcomes LEFT JOIN Ships
		ON Outcomes.ship = Ships.name
		WHERE Outcomes.result = 'sunk'
	)

	SELECT Classes.class, COUNT(OutcomeClasses.ship) AS [Sunks]
	FROM Classes LEFT JOIN OutcomeClasses
	ON Classes.class = OutcomeClasses.class 
	   OR Classes.class = OutcomeClasses.ship
	GROUP BY Classes.class
 *
 */

-- Exercise: 59 (Serge I: 2003-02-15) --

/* Calculate the cash balance of each buy-back center for the
 * database with money transactions being recorded not more than once a day.
 * Result set: point, balance.
 *
	SELECT inc_point AS [point], (ISNULL(inc_sum, 0.00) - ISNULL(out_sum, 0.00)) AS [Remain]
	FROM (
		SELECT point AS [inc_point], SUM(inc) AS [inc_sum]
		FROM Income_o
		GROUP BY point
	) AS [income]
	LEFT JOIN (
		SELECT point AS [out_point], SUM(out) AS [out_sum]
		FROM Outcome_o
		GROUP BY point
	) AS [outcome]
	ON income.inc_point = outcome.out_point
 *
 */

-- Exercise: 60 (Serge I: 2003-02-15) --

/* For the database with money transactions being recorded
 * not more than once a day, calculate the cash balance of each
 * buy-back center at the beginning of 4/15/2001.
 * Note: exclude centers not having any records before the specified date.
 * Result set: point, balance
 *
	SELECT inc_point AS [point], (ISNULL(inc_sum, 0.00) - ISNULL(out_sum, 0.00)) AS [Remain]
	FROM (
		SELECT point AS [inc_point], SUM(inc) AS [inc_sum]
		FROM Income_o
		WHERE date < '20010415'
		GROUP BY point
	) AS [income]
	LEFT JOIN (
		SELECT point AS [out_point], SUM(out) AS [out_sum]
		FROM Outcome_o
		WHERE date < '20010415'
		GROUP BY point
	) AS [outcome]
	ON income.inc_point = outcome.out_point	
 *
 */

-- Exercise: 61 (Serge I: 2003-02-14) --

/* For the database with money transactions being recorded not
 * more than once a day, calculate the total cash balance of all buy-back centers.
 *
	WITH RemainingSums AS(
		SELECT inc_point AS [point], (ISNULL(inc_sum, 0.00) - ISNULL(out_sum, 0.00)) AS [Remain]
		FROM (
			SELECT point AS [inc_point], SUM(inc) AS [inc_sum]
			FROM Income_o
			GROUP BY point
		) AS [income]
		LEFT JOIN (
			SELECT point AS [out_point], SUM(out) AS [out_sum]
			FROM Outcome_o
			GROUP BY point
		) AS [outcome]
		ON income.inc_point = outcome.out_point
	)

	SELECT SUM(Remain) AS [Remain]
	FROM RemainingSums
 *
 */

-- Exercise: 63 (Serge I: 2003-04-08) --

/* Find the names of different passengers that ever travelled
 * more than once occupying seats with the same number. 
 *
	SELECT name
	FROM Passenger 
	WHERE ID_psg IN(
		SELECT ID_psg
		FROM Pass_in_trip
		GROUP BY place, ID_psg
		HAVING COUNT(*) > 1
	)
 *
 */

-- Exercise: 66 (Serge I: 2003-04-09) --

/* For all days between 2003-04-01 and 2003-04-07
 * find the number of trips from Rostov.
 * Result set: date, number of trips. 
 *
	WITH Dates AS(
		SELECT CAST('20030401' AS datetime) AS [date]
			UNION ALL
		SELECT '20030402'
			UNION ALL
		SELECT '20030403'
			UNION ALL
		SELECT '20030404'
			UNION ALL
		SELECT '20030405'
			UNION ALL
		SELECT '20030406'
			UNION ALL
		SELECT '20030407'
	)

	SELECT Dates.date AS [Dt], COUNT(trip_no) AS [Qty]
	FROM (
		SELECT DISTINCT Trip.trip_no, Pass_in_trip.date
		FROM Trip INNER JOIN Pass_in_trip
		ON Trip.trip_no = Pass_in_trip.trip_no
		WHERE Trip.town_from = 'Rostov'
	) AS [TripsAndDates] RIGHT JOIN Dates
	ON TripsAndDates.date = Dates.date
	GROUP BY Dates.date
 *
 */

-- Exercise: 67 (Serge I: 2010-03-27) --

/* Find out the number of routes with the greatest number of flights (trips). 
 * Notes. 
 * 1) A - B and B - A are to be considered DIFFERENT routes.
 * 2) Use the Trip table only. 
 *
	WITH RoutesAndTrips AS (
		SELECT TOP(1) WITH TIES (town_from + town_to) AS [route],
								COUNT(trip_no) AS [num_trips]
		FROM Trip
		GROUP BY (town_from + town_to)
		ORDER BY num_trips DESC
	)

	SELECT COUNT(num_trips) AS [qty]
	FROM RoutesAndTrips;
 *
 */

-- Exercise: 68 (Serge I: 2010-03-27) --

/* Find out the number of routes with the greatest number of flights (trips).
 * Notes. 
 * 1) A - B and B - A are to be considered the SAME route.
 * 2) Use the Trip table only. 
 *
	WITH TopRoutesAndTrips AS (
		SELECT TOP(1) WITH TIES (town_from + town_to) AS [route],
								SUM(num_trips) AS [num_trips]
		FROM (
			SELECT COUNT(*) AS [num_trips], town_from, town_to 
			FROM trip
			WHERE town_from >= town_to
			GROUP BY town_from, town_to
				UNION ALL
			SELECT COUNT(*) AS [num_trips], town_to, town_from 
			FROM trip
			WHERE town_to > town_from
			GROUP BY town_from, town_to
		) AS [RoutesAndTrips]
		GROUP BY (town_from + town_to)
		ORDER BY num_trips DESC
	)

	SELECT COUNT(*) AS [qty]
	FROM TopRoutesAndTrips	
 *
 */

-- Exercise: 71 (Serge I: 2008-02-23) --

/* Find the PC makers with all personal computer models 
 * produced by them being present in the PC table.
 *
	SELECT maker
	FROM Product LEFT JOIN PC
	ON Product.model = PC.model
	WHERE type = 'PC'
	GROUP BY Product.maker
	HAVING COUNT(Product.model) = COUNT(PC.model)
 *
 */

-- Exercise: 73 (Serge I: 2009-04-17) --

/* For each country, determine the battles in which the ships
 * of this country did not participate.
 * Result set: country, battle. 
 *
	SELECT DISTINCT country, name
	FROM Classes, Battles
	EXCEPT
	SELECT country, battle AS [name]
	FROM Outcomes LEFT JOIN Ships ON Outcomes.ship = Ships.name
				  LEFT JOIN Classes ON Outcomes.ship = Classes.class
								       OR Ships.class = Classes.class
	GROUP BY country, battle
 *
 */

-- Exercise: 77 (Serge I: 2003-04-09) --

/* Find the days with the maximum number of flights departed from Rostov.
 * Result set: number of trips, date. 
 *
	WITH subquerry AS (
		SELECT COUNT(DISTINCT(Pass_in_trip.trip_no)) AS [Qty],
			   Pass_in_trip.date AS [date]
		FROM Trip INNER JOIN Pass_in_trip
		ON Trip.trip_no = Pass_in_trip.trip_no
		WHERE town_from = 'Rostov'
		GROUP BY Pass_in_trip.date
	)

	SELECT TOP(1) WITH TIES * 
	FROM subquerry 
	ORDER BY subquerry.[Qty] DESC
 *
 */

-- Exercise: 83 (dorin_larsen: 2006-03-14) --

/* Find out the names of the ships in the Ships table
 * that meet at least four criteria from the following list:
 * numGuns = 8, 
 * bore = 15,
 * displacement = 32000,
 * type = bb,
 * launched = 1915,
 * class = Kongo,
 * country = USA. 
 *
	SELECT name
	FROM Classes INNER JOIN Ships
	ON Classes.class = Ships.class
	WHERE (
		CASE WHEN numGuns = 8 THEN 1 ELSE 0 END +
		CASE WHEN bore = 15 THEN 1 ELSE 0 END +
		CASE WHEN displacement = 32000 THEN 1 ELSE 0 END +
		CASE WHEN type = 'bb' THEN 1 ELSE 0 END +
		CASE WHEN launched = 1915 THEN 1 ELSE 0 END +
		CASE WHEN Classes.class = 'Kongo' THEN 1 ELSE 0 END +
		CASE WHEN country = 'USA' THEN 1 ELSE 0 END 
	) > = 4
 *
 */

-- Exercise: 85 (Serge I: 2012-03-16) --

/* Get makers producing either printers only or
 * personal computers only; in case of PC 
 * manufacturers they should produce at least 3 models.
 *
	SELECT maker
	FROM product
	GROUP BY maker
	HAVING COUNT(DISTINCT type) = 1 and
		   (MIN(type) = 'Printer' or
		   (MIN(type) = 'PC' and COUNT(model) > 2))
 *
 */

-- Exercise: 86 (Serge I: 2012-04-20) --

/* For each maker, list the types of products he produces
 * in alphabetic order, using a slash ("/") as a delimiter.
 * Result set: maker, list of product types. 
 *
	SELECT maker,
		   CASE COUNT(DISTINCT type)
		   WHEN 2 THEN MIN(type) + '/' + MAX(type)
           WHEN 1 THEN MAX(type)
           WHEN 3 THEN 'Laptop/PC/Printer'
           END AS [types]
	FROM Product
	GROUP BY maker
 *
 */

-- Exercise: 88 (Serge I: 2003-04-29) --

/* Among those flying with a single airline find the names
 * of different passengers who have flown most often. 
 * Result set: passenger name, number of trips, and airline name. 
 *
	WITH ID_psgAndID_comp AS (
		SELECT TOP(1) WITH TIES 
			ID_psg, 
			MIN(ID_comp) AS ID_comp, 
			COUNT(*) AS [trip_Qty]
		FROM Pass_in_trip INNER JOIN Trip
		ON Pass_in_trip.trip_no = Trip.trip_no
		GROUP BY ID_psg
		HAVING COUNT(DISTINCT ID_comp) = 1
		ORDER BY trip_Qty DESC
	)

	SELECT Passenger.name, trip_Qty, Company.name AS [Company]
	FROM Passenger INNER JOIN ID_psgAndID_comp 
				   ON Passenger.ID_psg = ID_psgAndID_comp.ID_psg
				   INNER JOIN Company 
				   ON ID_psgAndID_comp.ID_comp = Company.ID_comp	
 *
 */

-- Exercise: 89 (Serge I: 2012-05-04) --

/* Get makers having most models in the Product
 * table, as well as those having least.
 * Output: maker, number of models. 
 *
	WITH MakersAndModels AS (
		SELECT maker, COUNT(model) AS [qty]
		FROM Product
		GROUP BY maker
	)

	SELECT maker, qty
	FROM MakersAndModels
	WHERE qty IN (
		SELECT MAX(qty)
		FROM MakersAndModels
			UNION
		SELECT MIN(qty)
		FROM MakersAndModels
	)
 *
 */

-- Exercise: 90 (Serge I: 2012-05-04) --

/* Display all records from the Product table except
 * for three rows with the smallest model numbers
 * and three ones with the greatest model numbers. 
 *
	SELECT *
	FROM Product
	WHERE model NOT IN(
		SELECT TOP(3) model
		FROM Product
		ORDER BY model
		UNION
		SELECT TOP(3) model
		FROM Product
		ORDER BY model DESC
	)
 *
 */

-- Exercise: 91 (Serge I: 2015-03-20) --

/* Determine the average quantity of paint per square
 * with an accuracy of two decimal places. 
 *
	WITH IDAndVolSums AS (
		SELECT B_Q_ID, sum(B_VOL) AS [vol_sum] 
		FROM utB
		GROUP BY b_q_id
			UNION
		SELECT q_id, 0 AS [vol_sum] 
		FROM utQ 
		WHERE q_id NOT IN (
			SELECT b_q_id 
			FROM utb
		)
	)

	SELECT CAST(AVG(CAST(vol_sum AS NUMERIC(6,2))) AS NUMERIC(6,2)) AS [avg_paint]
	FROM IDAndVolSums	
 *
 */

-- Exercise: 95 (qwrqwr: 2013-02-08) --

/* Using the Pass_in_Trip table, calculate for each airline:
 * 1) the number of performed flights;
 * 2) the number of plane types used;
 * 3) the number of different passengers that have been transported;
 * 4) the total number of passengers that have been transported by the company.
 * Output: airline name, 1), 2), 3), 4). 
 *
	SELECT name AS [company_name],
		   COUNT(DISTINCT CONVERT(CHAR(24), date) + CONVERT(CHAR(4), Trip.trip_no)) AS [flights],
           COUNT(DISTINCT plane) AS [planes],
           COUNT(DISTINCT ID_psg) AS [diff_psngrs],
           COUNT(*) AS [total_psngrs]
	FROM Company,Pass_in_trip,Trip
	WHERE Company.ID_comp=Trip.ID_comp 
		  AND Trip.trip_no=Pass_in_trip.trip_no
	GROUP BY Company.ID_comp, name
 *
 */

-- Exercise: 97 (qwrqwr: 2013-02-15) --

/* From the Laptop table, select rows fulfilling the following condition:
 * the values of the speed, ram, price, and screen columns can be arranged 
 * in such a way that each successive value exceeds two or more times the previous one.
 * Note: all known laptop characteristics are greater than zero.
 * Output: code, speed, ram, price, screen. 
 *
	SELECT code, speed, ram, price, screen
	FROM Laptop
	WHERE (screen/price >= 2 AND price/ram >= 2 AND ram/speed >= 2)
		OR (screen/price >= 2 AND price/speed >= 2 AND speed/ram >= 2)
		OR (screen/ram >= 2 AND ram/price >= 2 AND price/speed >= 2)
		OR (screen/ram >= 2 AND ram/speed >= 2 AND speed/price >= 2)
		OR (screen/speed >= 2 AND speed/ram >= 2 AND ram/price >= 2)
		OR (screen/speed >= 2 AND speed/price >= 2 AND price/ram >= 2)

		OR (price/speed >= 2 AND speed/ram >= 2 AND ram/screen >= 2)
		OR (price/speed >= 2 AND speed/screen >= 2 AND screen/ram >= 2)
		OR (price/ram >= 2 AND ram/speed >= 2 AND speed/screen >= 2)
		OR (price/ram >= 2 AND ram/screen >= 2 AND screen/speed >= 2)
		OR (price/screen >= 2 AND screen/ram >= 2 AND ram/speed >= 2)
		OR (price/screen >= 2 AND screen/speed >= 2 AND speed/ram >= 2)

		OR (speed/price >= 2 AND price/ram >= 2 AND ram/screen >= 2)
		OR (speed/price >= 2 AND price/screen >= 2 AND screen/ram >= 2)
		OR (speed/ram >= 2 AND ram/price >= 2 AND price/screen >= 2)
		OR (speed/ram >= 2 AND ram/screen >= 2 AND screen/price >= 2)
		OR (speed/screen >= 2 AND screen/ram >= 2 AND ram/price >= 2)
		OR (speed/screen >= 2 AND screen/price >= 2 AND price/ram >= 2)

		OR (ram/price >= 2 AND price/ram >= 2 AND speed/screen >= 2)
		OR (ram/price >= 2 AND price/screen >= 2 AND screen/speed >= 2)
		OR (ram/speed >= 2 AND speed/price >= 2 AND price/screen >= 2)
		OR (ram/speed >= 2 AND speed/screen >= 2 AND screen/price >= 2)
		OR (ram/screen >= 2 AND screen/speed >= 2 AND speed/price >= 2)
		OR (ram/screen >= 2 AND screen/price >= 2 AND price/speed >= 2)
 *
 */

-- Exercise: 102 (Serge I: 2003-04-29) --

/* Find the names of different passengers who travelled
 * between two towns only (one way or back and forth). 
 *
	WITH SubQuerry AS (
		SELECT id_psg 
		FROM Trip, Pass_in_trip
		WHERE Trip.trip_no = Pass_in_trip.trip_no
		GROUP BY id_psg
		HAVING COUNT (
			DISTINCT CASE
						  WHEN town_from <= town_to THEN town_from+town_to 
					      ELSE town_to+town_from 
				     END
		) = 1
	)

	SELECT name
	FROM Passenger INNER JOIN SubQuerry
	ON Passenger.ID_psg = SubQuerry.ID_psg	
 *
 */

-- Exercise: 133 (yuriy.rozhok: 2007-03-24) --

/* Let S be a subset of the set of integers. Let�s call "a hill with N on
 * its top" a sequence of members of S consisting of numbers less than N
 * arranged in ascending order from left to right and concatenated to a 
 * string without delimiters, followed by the same numbers arranged in
 * descending order, and the value of N lying in between. E. g., for 
 * S={1,2,...,10}, the hill with 5 on its top is represented as 123454321.
 * Assuming S consists of all company identifiers, put together a hill for
 * each company, with its ID forming the top of the hill. Consider all IDs
 * to be positive and note there is no data in the database that can cause
 * the hill sequence exceed 70 digits. Result set: id_comp, hill sequence
 *
	WITH ID_COMP AS (
		SELECT ID_comp, 
			   ROW_NUMBER() OVER (ORDER BY ID_comp ASC) AS [Number]
		FROM Company
	), REC_HILLS AS (
		SELECT Number, 
			   ID_comp, 
			   CAST('' AS varchar(8000)) AS [LeftHill],
			   CAST('' AS varchar(8000)) AS [RightHill]
		FROM ID_COMP
		WHERE Number = 1
			UNION ALL
		SELECT ID_COMP.Number,
			   ID_COMP.ID_comp,
			   REC_HILLS.LeftHill + CAST(REC_HILLS.ID_comp AS varchar(8000)),
		       CAST(REC_HILLS.ID_comp AS varchar(8000)) + REC_HILLS.RightHill
		FROM REC_HILLS INNER JOIN ID_COMP
		ON REC_HILLS.Number + 1 = ID_COMP.Number
	)

	SELECT ID_comp AS [top_id],
		   LeftHILL + CAST(ID_comp AS varchar(8000)) + RightHILL AS [hill]
	FROM REC_HILLS
 *
 */

-- Exercise: 140 (no_more: 2017-07-07) --

/* For the period from the earliest battle in the database to the
 * last one, find out how many battles happened during each decade. 
 * Result set: decade (in "1940s" format); number of battles.
 *
	WITH MinAndMaxDecades AS (
		SELECT MIN(YEAR(date) - YEAR(date) % 10) AS MinDecade,
			   MAX(YEAR(date) - YEAR(date) % 10) AS MaxDecade
		FROM Battles
	), AllDecades AS (
		SELECT MinDecade AS [Decades]
		FROM MinAndMaxDecades
			UNION ALL
		SELECT Decades + 10
		FROM AllDecades, MinAndMaxDecades
		WHERE Decades < MaxDecade
	)

	SELECT CAST(AllDecades.Decades AS varchar) + N's' AS [years],
		   COUNT(date) AS [battles]
	FROM AllDecades LEFT JOIN (
		SELECT Decades, date
		FROM AllDecades, Battles
		WHERE YEAR(date) BETWEEN Decades AND Decades + 9
	) AS BattlesInDecades
	ON AllDecades.Decades = BattlesInDecades.Decades
	GROUP BY AllDecades.Decades
 *
 */

-- Exercise: 155 (p�parome: 2005-12-02) --

/* Assuming there is no flight number greater than 65535, display the 
 * flight number and its binary representation (without leading zeroes).
 *
	WITH BinTrips AS (
		SELECT trip_no,
			   trip_no AS [Number],
			   CAST('' AS varchar(8000)) AS [trip_no_bit]
		FROM Trip
			UNION ALL
		SELECT trip_no,
			   Number / 2,
			   CONCAT(Number % 2, trip_no_bit)
		FROM BinTrips
		WHERE Number != 0
	)
 
	SELECT trip_no, trip_no_bit
	FROM BinTrips
	WHERE Number = 0
	ORDER BY trip_no
 *
 */